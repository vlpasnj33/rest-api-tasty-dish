package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/todo-list/internal/controller"
	"gitlab.com/vlpasnj33/todo-list/internal/middleware"
)

func NewRouter(controller *controller.Controller) *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		auth.POST("/sign-up", controller.SignUp)
		auth.POST("/sign-in", controller.SignIn)
	}

	api := router.Group("/api", middleware.UserIdentity)
	{
		lists := api.Group("/lists")
		{
			lists.POST("/", controller.CreateList)
			lists.GET("/", controller.GetAllLists)
			lists.GET("/:id", controller.GetListById)
			lists.PUT("/:id", controller.UpdateList)
			lists.DELETE("/:id", controller.DeleteList)

			items := lists.Group(":id/items") 
			{
				items.POST("/", controller.CreateItem)
				items.GET("/", controller.GetAllItems)
			}
		}
		items := api.Group("items")
		{
			items.GET("/:id", controller.GetItemById)
			items.PUT("/:id", controller.UpdateItem)
			items.DELETE("/:id", controller.DeleteItem)
		}
	}
	
	return router
}