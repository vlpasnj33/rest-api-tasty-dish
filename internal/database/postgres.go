package database

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
)

const (
	UsersTable      = "users"
	TodoListsTable  = "todo_lists"
	UsersListsTable = "users_lists"
	TodoItemsTable  = "todo_items"
	ListsItemsTable = "lists_items"
)

type Config struct {	
	Host     string
	Port     string
	User	 string
	Password string
	DBName   string
	SSLMode  string
}


func NewPostgresDb(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", 
						fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
									cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.DBName, cfg.SSLMode,
									))
	if err != nil {
		log.Fatalf("failed to init db: %s", err.Error())
	}

	err = db.Ping()
	if err != nil {
		log.Fatalf("failed db ping: %s", err.Error())
	}

	return db, nil
}