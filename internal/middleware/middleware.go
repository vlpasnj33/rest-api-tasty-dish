package middleware

import (
	"errors"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/todo-list/internal/service"
	"gitlab.com/vlpasnj33/todo-list/pkg/helpers"
)

const (
	authHeader = "Authorization"
	userCtx = "userId"
)

func UserIdentity(ctx *gin.Context) {
	header := ctx.GetHeader(authHeader)
	if header == "" {
		helpers.NewErrorResponse(ctx, http.StatusUnauthorized, "empty auth header")
		return
	}

	headerParts := strings.Split(header, " ")
	if len(headerParts) != 2 || headerParts[0] != "Bearer" {
		helpers.NewErrorResponse(ctx, http.StatusUnauthorized, "invalid auth header")
		return
	}

	if len(headerParts[1]) == 0 {
		helpers.NewErrorResponse(ctx, http.StatusUnauthorized, "token is empty")
		return
	}

	authService := &service.AuthService{} 
	userId, err := authService.ParseToken(headerParts[1])
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusUnauthorized, err.Error())
		return
	}

	ctx.Set(userCtx, userId)
}

func GetUserId(ctx *gin.Context) (string, error) {
	id, ok := ctx.Get(userCtx)
	if !ok {
		return "", errors.New("user id not found")
	}

	idStr, ok := id.(string)
	if !ok {
		return "", errors.New("user id is of invalid type")
	}

	return idStr, nil
}