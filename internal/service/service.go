package service

import (
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/internal/repository"
)

type Auth interface {
	CreateUser(user model.User) (string, error)
	GenerateToken(email, password string) (string, error)
	ParseToken(token string) (string, error)
}

type TodoList interface {
	CreateList(userId string, list model.TodoList) (string, error)
	GetAll(userId string) ([]model.TodoList, error)
	GetById(userId, listId string) (model.TodoList, error)
	Update(userId, listId string, input model.UpdateListInput) error
	Delete(userId, listId string) error
}

type TodoItem interface {
	CreateItem(userId, listId string, item model.TodoItem) (string, error)
	GetAll(userId, listId string) ([]model.TodoItem, error)
	GetById(userId, itemId string) (model.TodoItem, error)
	Update(userId, itemId string, input model.UpdateItemInput) error
	Delete(userId, itemId string) error
}

type Service struct {
	Auth
	TodoList
	TodoItem
}

func NewService(r *repository.Repository) *Service {
	return &Service{
		Auth: 		NewAuthService(r.Auth),
		TodoList:	NewTodoListService(r.TodoList),
		TodoItem: 	NewTodoItemService(r.TodoItem, r.TodoList),
	}
}