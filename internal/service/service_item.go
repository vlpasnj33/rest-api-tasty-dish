package service

import (
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/internal/repository"
)

type TodoItemService struct {
	repos 		repository.TodoItem
	listRepos	repository.TodoList
}

func NewTodoItemService(repo repository.TodoItem, listRepo repository.TodoList) *TodoItemService {
	return &TodoItemService{repos: repo, listRepos: listRepo}
}

func(s *TodoItemService) CreateItem(userId, listId string, item model.TodoItem) (string, error) {
	_, err := s.listRepos.GetById(userId, listId)
	if err != nil {
		return "", err
	}
	return s.repos.CreateItem(listId, item)
}

func(s *TodoItemService) GetAll(userId, listId string) ([]model.TodoItem, error) {
	return s.repos.GetAll(userId, listId)
}

func(s *TodoItemService) GetById(userId, itemId string) (model.TodoItem, error) {
	return s.repos.GetById(userId, itemId)
}

func(s *TodoItemService) Update(userId, itemId string, input model.UpdateItemInput) error {
	return s.repos.Update(userId, itemId, input)
}

func(s *TodoItemService) Delete(userId, itemId string) error {
	return s.repos.Delete(userId, itemId)
}