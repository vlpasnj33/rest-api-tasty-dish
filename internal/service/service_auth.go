package service

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/internal/repository"
)

const (
	salt 	  = "fadskflamsdkklam3?"
	signInKey = "opjgajdspmcasdklcd" 
)

type tokenClaims struct {
	jwt.RegisteredClaims
	UserId string `json:"user_id"`
}

type AuthService struct {
	repo repository.Auth
}

func NewAuthService(repository repository.Auth) *AuthService {
	return &AuthService{repo: repository}
}

func(s *AuthService) CreateUser(user model.User) (string, error) {
	user.Password = generatePasswordHash(user.Password)
	return s.repo.CreateUser(user)
}

func(s *AuthService) GenerateToken(email, password string) (string, error) {
	user, err := s.repo.GetUser(email, generatePasswordHash(password))
	if err != nil {
		return "", err
	}

	claims := tokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(12 * time.Hour)),
			IssuedAt: jwt.NewNumericDate(time.Now()),
		}, 
		UserId: user.Id,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(signInKey))
}

func(s *AuthService) ParseToken(accessToken string) (string, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("invalid signing method")
			}
			return []byte(signInKey), nil
	})
	if err != nil {
		return "", err
	}
	
	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return "", errors.New("token claims are not type *tokenClaims")
	}

	return claims.UserId, nil
}

func generatePasswordHash(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))
	
	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}