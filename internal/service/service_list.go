package service

import (
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/internal/repository"
)

type TodoListService struct {
	repos repository.TodoList
}

func NewTodoListService(repo repository.TodoList) *TodoListService {
	return &TodoListService{repos: repo}
}

func(s *TodoListService) CreateList(userId string, list model.TodoList) (string, error) {
	return s.repos.CreateList(userId, list)
}

func(s *TodoListService) GetAll(userId string) ([]model.TodoList, error) {
	return s.repos.GetAll(userId)
}

func(s *TodoListService) GetById(userId, listId string) (model.TodoList, error) {
	return s.repos.GetById(userId, listId)
}

func(s *TodoListService) Update(userId, listId string, input model.UpdateListInput) error {
	if err := input.Validate(); err != nil {
		return err
	}
	return s.repos.Update(userId, listId, input)
}

func(s *TodoListService) Delete(userId, listId string) error {
	return s.repos.Delete(userId, listId)
}