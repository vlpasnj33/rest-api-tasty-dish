package repository

import (
	"fmt"
	"strings"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/vlpasnj33/todo-list/internal/database"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
)

type TodoItemRepository struct {
	db *sqlx.DB
}

func NewTodoItemRepository(database *sqlx.DB) *TodoItemRepository {
	return &TodoItemRepository{db: database}
}

func(r *TodoItemRepository) CreateItem(listId string, item model.TodoItem) (string, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return "", err
	}

	itemId := uuid.New().String()
	createItemQuery := fmt.Sprintf(`INSERT INTO %s (id, title, description) VALUES ($1, $2, $3) RETURNING id`,
									database.TodoItemsTable)
	row := tx.QueryRow(createItemQuery, itemId, item.Title, item.Description)
	err = row.Scan(&itemId)
	if err != nil {
		tx.Rollback()
		return "", err
	}

	id := uuid.New().String()
	creteListItemsQuery := fmt.Sprintf(`INSERT INTO %s (id, list_id, item_id) VALUES ($1, $2, $3)`, database.ListsItemsTable)
	_, err = tx.Exec(creteListItemsQuery, id, listId, itemId)
	if err != nil {
		tx.Rollback()
		return "", err
	}
	
	return itemId, tx.Commit()
}

func(r *TodoItemRepository) GetAll(userId, listId string) ([]model.TodoItem, error) {
	var items []model.TodoItem
	query := fmt.Sprintf(`SELECT ti.id, ti.title, ti.description, ti.done
						  FROM %s ti
						  INNER JOIN %s li ON li.item_id = ti.id
						  INNER JOIN %s ul ON ul.list_id = li.list_id
						  WHERE li.list_id=$1 AND ul.user_id=$2`,
						  database.TodoItemsTable, database.ListsItemsTable, database.UsersListsTable,
						 )	
	if err := r.db.Select(&items, query, listId, userId); err != nil {
		return nil, err
	}

	return items, nil
}

func(r *TodoItemRepository) GetById(userId, itemId string) (model.TodoItem, error) {
	var item model.TodoItem
	query := fmt.Sprintf(`SELECT ti.id, ti.title, ti.description, ti.done
						  FROM %s ti 
						  INNER JOIN %s li ON li.item_id = ti.id
						  INNER JOIN %s ul ON ul.list_id = li.list_id
						  WHERE ti.id=$1 AND ul.user_id=$2`,
						  database.TodoItemsTable, database.ListsItemsTable, database.UsersListsTable, 
						)	
	if err := r.db.Get(&item, query, itemId, userId); err != nil {
		return item, err
	}

	return item, nil
}

func(r *TodoItemRepository) Update(userId, itemId string, input model.UpdateItemInput) error {
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if input.Title != nil {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argId))
		args = append(args, *input.Title)
		argId++
	}

	if input.Description != nil {
		setValues = append(setValues, fmt.Sprintf("description=$%d", argId))
		args = append(args, *input.Description)
		argId++
	}

	if input.Done != nil {
		setValues = append(setValues, fmt.Sprintf("done=$%d", argId))
		args = append(args, *input.Done)
		argId++
	}

	setQuery := strings.Join(setValues, ", ")

	query := fmt.Sprintf(`UPDATE %s ti SET %s FROM %s li, %s ul
						  WHERE ti.id = li.item_id AND li.list_id = ul.list_id
						  						   AND ul.user_id=$%d AND ti.id=$%d`,
						database.TodoItemsTable, setQuery, database.ListsItemsTable, 
						database.UsersListsTable, argId, argId+1)
	args = append(args, userId, itemId)

	_, err := r.db.Exec(query, args...)

	return err
}

func(r *TodoItemRepository) Delete(userId, itemId string) error {
	query := fmt.Sprintf(`DELETE FROM %s ti USING %s li, %s ul
						  WHERE ti.id = li.item_id AND li.list_id = ul.list_id 
						  						   AND ul.user_id=$1 AND ti.id=$2`,
						database.TodoItemsTable, database.ListsItemsTable, database.UsersListsTable)
	_, err := r.db.Exec(query, userId, itemId)

	return err 	
}