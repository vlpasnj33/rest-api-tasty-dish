package repository

import (
	"fmt"
	"strings"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/vlpasnj33/todo-list/internal/database"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
)

type TodoListRepository struct {
	db *sqlx.DB
}

func NewTodoListRepository(database *sqlx.DB) *TodoListRepository {
	return &TodoListRepository{db: database}
}

func(r *TodoListRepository) CreateList(userId string, list model.TodoList) (string, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return "", err
	}

	uuId := uuid.New().String()
	listQuery := fmt.Sprintf("INSERT INTO %s (id, title, description) VALUES ($1, $2, $3) RETURNING id", database.TodoListsTable)
	row := tx.QueryRow(listQuery, uuId, list.Title, list.Description)
    if err := row.Scan(&uuId); err != nil {
        tx.Rollback()
		return "empty uuid", err
    }

	id := uuid.New().String()
	usersListQuery := fmt.Sprintf("INSERT INTO %s (id, user_id, list_id) VALUES ($1, $2, $3)", database.UsersListsTable)
	_, err = tx.Exec(usersListQuery, id, userId, uuId)
	if err != nil {
		tx.Rollback()
		return "err", err
	}

	return id, tx.Commit()
}

func(r *TodoListRepository) GetAll(userId string) ([]model.TodoList, error) {
	var lists []model.TodoList
	query := fmt.Sprintf(`SELECT tl.id, tl.title, tl.description 
						  FROM %s tl 
						  INNER JOIN %s ul on tl.id = ul.list_id 
						  WHERE ul.user_id = $1`,
						database.TodoListsTable, database.UsersListsTable)
	
	err := r.db.Select(&lists, query, userId)

	return lists, err
}

func(r *TodoListRepository) GetById(userId, listId string) (model.TodoList, error) {
	var list model.TodoList
	query := fmt.Sprintf(`SELECT tl.id, tl.title, tl.description 
						  FROM %s tl 
						  INNER JOIN %s ul on tl.id = ul.list_id 
						  WHERE ul.user_id = $1 AND ul.list_id = $2`,
						database.TodoListsTable, database.UsersListsTable)

	err := r.db.Get(&list, query, userId, listId)

	return list, err
}

func(r *TodoListRepository) Update(userId, listId string, input model.UpdateListInput) error {
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if input.Title != nil {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argId))
		args = append(args, *input.Title)
		argId++
	}

	if input.Description != nil {
		setValues = append(setValues, fmt.Sprintf("description=$%d", argId))
		args = append(args, *input.Description)
		argId++
	}

	setQuery := strings.Join(setValues, ", ")

	query := fmt.Sprintf(`UPDATE %s tl SET %s FROM %s ul 
						  WHERE tl.id = ul.list_id AND ul.list_id=$%d AND ul.user_id=$%d`,
						  database.TodoListsTable, setQuery, database.UsersListsTable, argId, argId+1)
	
	args = append(args, listId, userId)

	_, err := r.db.Exec(query, args...)
	return err
}

func(r *TodoListRepository) Delete(userId, listId string) error {
	query := fmt.Sprintf("DELETE FROM %s tl USING %s ul WHERE tl.id = ul.list_id AND ul.user_id=$1 AND ul.list_id=$2",
						database.TodoListsTable, database.UsersListsTable)
						
	_, err := r.db.Exec(query, userId, listId)

	return err
}