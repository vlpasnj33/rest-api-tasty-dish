package repository

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/vlpasnj33/todo-list/internal/database"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
)

type AuthRepository struct {
	DB *sqlx.DB
}

func NewAuthRepository(db *sqlx.DB) *AuthRepository {
	return &AuthRepository{DB: db}
}

func (r AuthRepository) CreateUser(user model.User) (string, error) {
	id := uuid.New().String()
    query := fmt.Sprintf("INSERT INTO %s (id, email, username, password_hash) VALUES ($1, $2, $3, $4)", database.UsersTable)

    _, err := r.DB.Exec(query, id, user.Email, user.Username, user.Password)
    if err != nil {
        return "", err
    }

    return id, nil
}

func (r *AuthRepository) GetUser(email, password string) (model.User, error) {
	var user model.User

	query := fmt.Sprintf("SELECT id FROM %s WHERE email=$1 AND password_hash=$2", database.UsersTable)
	err := r.DB.Get(&user, query, email, password)

	return user, err
}
