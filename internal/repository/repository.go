package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
)

type Auth interface {
	CreateUser(user model.User) (string, error)
	GetUser(email, password string) (model.User, error)
}

type TodoList interface {
	CreateList(userId string, list model.TodoList) (string, error)
	GetAll(userId string) ([]model.TodoList, error)
	GetById(userId, listId string) (model.TodoList, error)
	Update(userId, listId string, input model.UpdateListInput) error 
	Delete(userId, listId string) error
}

type TodoItem interface {
	CreateItem(listId string, item model.TodoItem) (string, error)
	GetAll(userId, listId string) ([]model.TodoItem, error)
	GetById(userId, itemId string) (model.TodoItem, error)
	Update(userId, itemId string, input model.UpdateItemInput) error
	Delete(userId, itemId string) error
}

type Repository struct {
	Auth
	TodoList
	TodoItem
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Auth: NewAuthRepository(db),
		TodoList: NewTodoListRepository(db),
		TodoItem: NewTodoItemRepository(db),
	}
}