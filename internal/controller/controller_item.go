package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/todo-list/internal/middleware"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/pkg/helpers"
)

func(c *Controller) CreateItem(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	listId := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	var input model.TodoItem
	if err := ctx.BindJSON(&input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	} 

	id, err := c.services.TodoItem.CreateItem(userId, listId, input)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

func(c *Controller) GetAllItems(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	listId := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	items, err := c.services.TodoItem.GetAll(userId, listId)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, items)
}

func(c *Controller) GetItemById(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	itemId := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	item, err := c.services.TodoItem.GetById(userId, itemId)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, item)
}

func(c *Controller) UpdateItem(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	id := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	var input model.UpdateItemInput
	if err := ctx.BindJSON(&input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	if err := c.services.TodoItem.Update(userId, id, input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, helpers.StatusResponse{Status: "Ok"})
}

func(c *Controller) DeleteItem(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	itemId := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	err = c.services.TodoItem.Delete(userId, itemId)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, helpers.StatusResponse{Status: "ok"})
}