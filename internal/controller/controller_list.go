package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/todo-list/internal/middleware"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/pkg/helpers"
)

func(c *Controller) CreateList(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	var input model.TodoList
	if err := ctx.BindJSON(&input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	id, err := c.services.TodoList.CreateList(userId, input)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}


	ctx.JSON(http.StatusCreated, map[string]interface{}{
		"id": id,
	})
}

type getAllListsResponse struct {
	Data []model.TodoList `json:"data"`
}

func(c *Controller) GetAllLists(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	lists, err := c.services.TodoList.GetAll(userId)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, getAllListsResponse{
		Data: lists,
	})
}

func(c *Controller) GetListById(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	listId := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	list, err := c.services.TodoList.GetById(userId, listId)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, list)
}

func(c *Controller) UpdateList(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	id := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	var input model.UpdateListInput
	if err := ctx.BindJSON(&input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	if err := c.services.TodoList.Update(userId, id, input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, helpers.StatusResponse{Status: "Ok"})
}

func(c *Controller) DeleteList(ctx *gin.Context) {
	userId, err := middleware.GetUserId(ctx)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	listId := ctx.Param("id")
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	err = c.services.TodoList.Delete(userId, listId)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, helpers.StatusResponse{
		Status: "OK",
	})
}