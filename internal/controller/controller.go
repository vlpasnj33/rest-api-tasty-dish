package controller

import "gitlab.com/vlpasnj33/todo-list/internal/service"

type Controller struct {
	services *service.Service
}

func NewController(s *service.Service) *Controller {
	return &Controller{services: s}
}