package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/vlpasnj33/todo-list/internal/model"
	"gitlab.com/vlpasnj33/todo-list/pkg/helpers"
)

func (c *Controller) SignUp(ctx *gin.Context) {
	var input model.User

	if err := ctx.BindJSON(&input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	id, err := c.services.Auth.CreateUser(input)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

type signIn struct {
	Email		string	`json:"email" binding:"required"`
	Password	string 	`json:"password" binding:"required"`
}

func (c *Controller) SignIn(ctx *gin.Context) {
	var input signIn

	if err := ctx.BindJSON(&input); err != nil {
		helpers.NewErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	token, err := c.services.Auth.GenerateToken(input.Email, input.Password)
	if err != nil {
		helpers.NewErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"token": token,
	})
}

