package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	server "gitlab.com/vlpasnj33/todo-list"
	"gitlab.com/vlpasnj33/todo-list/config"
	"gitlab.com/vlpasnj33/todo-list/internal/controller"
	"gitlab.com/vlpasnj33/todo-list/internal/database"
	"gitlab.com/vlpasnj33/todo-list/internal/repository"
	_ "gitlab.com/vlpasnj33/todo-list/internal/repository"
	"gitlab.com/vlpasnj33/todo-list/internal/router"
	"gitlab.com/vlpasnj33/todo-list/internal/service"
)

func main() {
	if err := config.InitConfig(); err != nil {
		logrus.Fatalf("error with init config: %s", err.Error())
	}

	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("error loading env var: %s", err.Error())
	}

	db, err := database.NewPostgresDb(database.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		User: 	  viper.GetString("db.user"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
		Password: os.Getenv("DB_PASSWORD"),
	})

	if err != nil {
		logrus.Fatalf("failed init: %s", err.Error())
	}

	repository := repository.NewRepository(db)

	service := service.NewService(repository)

	controller := controller.NewController(service)

	routes := router.NewRouter(controller) 

	srv := new(server.Server)
	go func () {
		if err := srv.Run(viper.GetString("port"), routes); err != nil {
			logrus.Fatalf("error to starting server: %s", err.Error())
		}
	}()

	logrus.Print("Server is working...")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("Shutdown")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

	if err := db.Close(); err != nil {
		logrus.Errorf("error occured on db connection close: %s", err.Error())
	}
}