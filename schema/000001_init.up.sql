CREATE TABLE users (
    id uuid NOT NULL UNIQUE,
    email varchar(255) NOT NULL UNIQUE,
    username varchar(255) NOT NULL,
    password_hash varchar(255) NOT NULL
);

CREATE TABLE todo_lists (
    id uuid NOT NULL UNIQUE,
    title varchar(255) NOT NULL,
    description varchar(255) 
);

CREATE TABLE users_lists (
    id uuid NOT NULL UNIQUE,
    user_id uuid REFERENCES users (id) ON DELETE CASCADE NOT NULL,
    list_id uuid REFERENCES todo_lists (id) ON DELETE CASCADE NOT NULL
);

CREATE TABLE todo_items (
    id uuid NOT NULL UNIQUE,
    title varchar(255) NOT NULL,
    description varchar(255),
    done boolean NOT NULL DEFAULT FALSE
);

CREATE TABLE lists_items (
    id uuid NOT NULL UNIQUE,
    item_id uuid REFERENCES todo_items (id) ON DELETE CASCADE NOT NULL,
    list_id uuid REFERENCES todo_lists (id) ON DELETE CASCADE NOT NULL
);